# Node.js 이미지를 기반으로 한다냥
FROM node:14

# 작업 디렉토리를 설정한다냥
WORKDIR /usr/src/app

# 의존성 파일을 복사하고 설치한다냥
COPY package*.json ./
RUN npm install

# 소스 코드를 복사한다냥
COPY ./src ./src

# TypeScript를 컴파일한다냥
RUN npm run build

# 냥냥챗봇을 실행하는 명령어다냥
CMD ["npm", "start"]