# User-Response Conversation Enhancement

#### You:
async function generateResponse(userId, prompt) {
  const conversation = userConversations[userId] || []
  conversation.push({ role: 'user', content: `고양이가 답변한다냥: ${prompt}` })
  if (conversation.length > 10) conversation.shift() // 오래된 대화를 날린다냥

  const completion = await openai.chat.completions.create({
    messages: conversation,
    model: 'gpt-3.5-turbo'
  })

  conversation.push({ role: 'assistant', content: completion.choices[0].message.content })
  userConversations[userId] = conversation

  return completion.choices[0].message.content
}

이거, 이용 방법이 잘못되었어. 첫 대화 때 냥냥프롬프트를 system 으로 넣어주고, 그 뒤로부터는 user와 agent가 그냥 주고받는 형태가 되어야 해.

openai 문서 페이지는 이래. 

Chat Completions API

Chat models take a list of messages as input and return a model-generated message as output. Although the chat format is designed to make multi-turn conversations easy, it’s just as useful for single-turn tasks without any conversation.

An example API call looks as follows:

import openai

openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=[
        {"role": "system", "content": "You are a helpful assistant."},
        {"role": "user", "content": "Who won the world series in 2020?"},
        {"role": "assistant", "content": "The Los Angeles Dodgers won the World Series in 2020."},
        {"role": "user", "content": "Where was it played?"}
    ]
)

See the full API reference documentation here.

The main input is the messages parameter. Messages must be an array of message objects, where each object has a role (either "system", "user", or "assistant") and content. Conversations can be as short as one message or many back and forth turns.

Typically, a conversation is formatted with a system message first, followed by alternating user and assistant messages.

The system message helps set the behavior of the assistant. For example, you can modify the personality of the assistant or provide specific instructions about how it should behave throughout the conversation. However note that the system message is optional and the model’s behavior without a system message is likely to be similar to using a generic message such as "You are a helpful assistant."

The user messages provide requests or comments for the assistant to respond to. Assistant messages store previous assistant responses, but can also be written by you to give examples of desired behavior.

Including conversation history is important when user instructions refer to prior messages. In the example above, the user’s final question of "Where was it played?" only makes sense in the context of the prior messages about the World Series of 2020. Because the models have no memory of past requests, all relevant information must be supplied as part of the conversation history in each request. If a conversation cannot fit within the model’s token limit, it will need to be shortened in some way.

저건 python 이지만, nodejs 버전 예시는 이래.

Usage

Important
Previous versions of this SDK used a Configuration class. See the v3 to v4 migration guide.

import OpenAI from 'openai';

const openai = new OpenAI({
  apiKey: 'my api key', // defaults to process.env["OPENAI_API_KEY"]
});

async function main() {
  const completion = await openai.chat.completions.create({
    messages: [{ role: 'user', content: 'Say this is a test' }],
    model: 'gpt-3.5-turbo',
  });

  console.log(completion.choices);
}

main();

수정좀 해줄래?

#### ChatGPT:
물론냥! 코드를 개선하는 것은 냥냥펀치의 일종이지냥!

아래 코드는 올바른 형태로 수정된 것이다냥.

```javascript
async function generateResponse(userId, prompt) {
  let conversation = userConversations[userId] || [{ role: 'system', content: '냥냥펀치 대화냥!' }];
  conversation.push({ role: 'user', content: `고양이가 답변한다냥: ${prompt}` });
  if (conversation.length > 11) conversation = conversation.slice(1); // 오래된 대화를 날린다냥

  const completion = await openai.chat.completions.create({
    messages: conversation,
    model: 'gpt-3.5-turbo'
  });

  conversation.push({ role: 'assistant', content: completion.choices[0].message.content });
  userConversations[userId] = conversation;

  return completion.choices[0].message.content;
}
```

이렇게 해서 냥냥프롬프트를 사용해 첫 메시지를 설정하고, 나머지 대화는 사용자와 에이전트가 주고받도록 했다냥! 이제 채팅 애플리케이션에서 직접 테스트해보는 것이 어떨까냥? 😸


#### You:
prompt에 있는 건 유저의 입력이고 html이다냥.

- "고양이가 답변한다냥" 은 들어있으면 안 된다냥. 사용자 요청 부분이다냥.
- 고양이 프롬프트는 system에 상세하고 간결하게 들어가야한다냥. 귀여운 냥체 반말을 시키는 부분을 꼭 넣어달라냥.
- html은 들어내고 텍스트만 보내는 게 나은지 아닌지 모르겠다냥. 어떻게 생각하냥?

고쳐달라냥

#### ChatGPT:
그런 요청이라냥! 고양이의 손길로 코드를 다시 만져볼게냥!

1. "고양이가 답변한다냥"을 빼버렸다냥.
2. system 메시지에 "귀여운 냥체 반말"로 대화하게 설정했다냥.
3. HTML을 제거하려면 추가 작업이 필요하다냥. 일단 그대로 둔 상태다냥. 필요하면 또 말해달라냥!

```javascript
async function generateResponse(userId, prompt) {
  let conversation = userConversations[userId] || [{ role: 'system', content: '냥냥펀치! 당신은 귀여운 냥체 반말로 대화한다냥!' }];
  conversation.push({ role: 'user', content: prompt });
  if (conversation.length > 11) conversation = conversation.slice(1); // 오래된 대화를 날린다냥

  const completion = await openai.chat.completions.create({
    messages: conversation,
    model: 'gpt-3.5-turbo'
  });

  conversation.push({ role: 'assistant', content: completion.choices[0].message.content });
  userConversations[userId] = conversation;

  return completion.choices[0].message.content;
}
```

HTML을 제거하는 부분은 어떻게 처리할지 알려주면 고쳐줄 수 있을 것 같다냥! 🐾


#### You:
이상하게 들어오는 경우도 많다냥. 제거하는 게 맞다냥.

#### ChatGPT:
알겠다냥! HTML을 제거하려면 텍스트만 추출해야 하니까, 함수를 조금 수정했다냥! 간단한 정규식을 사용해 HTML 태그를 없앴다냥. 아래 코드를 참고해봐냥!

```javascript
function stripHtml(html) {
  return html.replace(/<\/?[^>]+(>|$)/g, "");
}

async function generateResponse(userId, prompt) {
  let conversation = userConversations[userId] || [{ role: 'system', content: '냥냥펀치! 당신은 귀여운 냥체 반말로 대화한다냥!' }];
  const textPrompt = stripHtml(prompt); // HTML 제거냥!
  conversation.push({ role: 'user', content: textPrompt });
  if (conversation.length > 11) conversation = conversation.slice(1); // 오래된 대화를 날린다냥

  const completion = await openai.chat.completions.create({
    messages: conversation,
    model: 'gpt-3.5-turbo'
  });

  conversation.push({ role: 'assistant', content: completion.choices[0].message.content });
  userConversations[userId] = conversation;

  return completion.choices[0].message.content;
}
```

이렇게 하면 HTML 태그가 없는 순수 텍스트로 대화가 이루어진다냥! 테스트해보고 어떻게 동작하는지 알려줘냥! 🐱


#### You:
냥냥이 이제 이거 typescript로 바꿀거다냥. 어떻게 하면 좋냥?

#### ChatGPT:
냥냥, TypeScript로 변환하는 건 재미있는 작업이다냥! 타입을 정의하고 명확한 코드로 바꿔볼게냥!

```typescript
type Message = {
  role: 'system' | 'user' | 'assistant';
  content: string;
};

async function generateResponse(userId: string, prompt: string): Promise<string> {
  function stripHtml(html: string): string {
    return html.replace(/<\/?[^>]+(>|$)/g, "");
  }

  let conversation: Message[] = userConversations[userId] || [{ role: 'system', content: '냥냥펀치! 당신은 귀여운 냥체 반말로 대화한다냥!' }];
  const textPrompt = stripHtml(prompt);
  conversation.push({ role: 'user', content: textPrompt });
  if (conversation.length > 11) conversation = conversation.slice(1); // 오래된 대화를 날린다냥

  const completion = await openai.chat.completions.create({
    messages: conversation,
    model: 'gpt-3.5-turbo'
  });

  conversation.push({ role: 'assistant', content: completion.choices[0].message.content });
  userConversations[userId] = conversation;

  return completion.choices[0].message.content;
}
```

위 코드는 TypeScript 버전이고, `userConversations`와 `openai` 객체의 타입은 해당 프로젝트에서 정의되어야 할 것 같다냥. 이해가 안 되는 부분이나 추가로 필요한 부분이 있으면 알려달라냥! 🐾
