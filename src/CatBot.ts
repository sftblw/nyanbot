import generator, { MegalodonInterface } from 'megalodon';
import OpenAIApi from 'openai';
import { User } from './User';
import { ACCESS_TOKEN, ANCESTOR_DIALOGUE_PROMPT_MESSAGE, ANCESTOR_HISTORY_LIMIT, CURRENT_DIALOGUE_PROMPT_MESSAGE, DESCENDANT_DIALOGUE_PROMPT_MESSAGE, DESCENDANT_HISTORY_LIMIT, INITIAL_PROMPT_MESSAGE, INSTANCE_URL, MAX_TOKEN, MODEL_VERSION, OPENAI_API_KEY, USER_CACHE_MAX_SIZE, USER_CACHE_TTL_HOURS, USER_DIALOGUE_PROMPT_MESSAGE, USER_MESSAGE_HISTORY_LIMIT } from './EnvVars';
import {LRUCache} from 'lru-cache';
import Account, { ChatAccount } from './Account';
import { ChatMessage } from './ChatMessage';
/**
 * HTML 태그를 제거한다냥.
 * @param html HTML 문자열이다냥.
 * @returns HTML 태그가 제거된 문자열을 반환한다냥.
 */
function stripHtml(html: string): string {
    return html.replace(/<\/?[^>]+(>|$)/g, "");
}
function asAccountSanitize(realAcct: Entity.Account | Entity.Mention): Account {
    return {
        "acct": realAcct.acct,
        "display_name": (realAcct as Entity.Account).display_name ?? "(null)",
        "id": realAcct.id,
        "username": realAcct.username
    }
}

function asAccountSanitizeChat(realAcct: Entity.Account | Entity.Mention): ChatAccount {
    return {
        "acct": realAcct.acct,
        "display_name": (realAcct as Entity.Account).display_name ?? "(null)"
        // "username": realAcct.username
    }
}

function toStatusData(status: Entity.Status): ChatMessage {
    const user = asAccountSanitize(status.account);
    const prompt = status.content;
    const textPrompt = stripHtml(prompt);
    
    const jsonPrompt = JSON.stringify({
        'content': textPrompt,
        'author': JSON.stringify(user),
        'created_at': status.created_at,
        'status_mentions': status.mentions.map(it => asAccountSanitizeChat(it))
    });

    return {'role': 'user', 'content': jsonPrompt};
}

/**
 * 냥냥펀치! CatBot은 귀여운 냥체로 대화를 처리한다냥.
 */
export class CatBot {
    instanceUrl: string;
    accessToken: string;
    openaiApiKey: string;
    openai: OpenAIApi;
    client: MegalodonInterface;
    clientRest: MegalodonInterface;
    users: LRUCache<string, User>;

    constructor() {
        this.instanceUrl = INSTANCE_URL;
        this.accessToken = ACCESS_TOKEN;
        this.openaiApiKey = OPENAI_API_KEY;
        this.openai = new OpenAIApi({ apiKey: this.openaiApiKey });
        this.client = generator('mastodon', 'wss://' + this.instanceUrl, this.accessToken);
        this.clientRest = generator('mastodon', 'https://' + this.instanceUrl, this.accessToken);

        this.users = new LRUCache<string, User>({
            max: USER_CACHE_MAX_SIZE, // 캐시의 최대 크기다냥
            ttl: 1000 * 60 * 60 * USER_CACHE_TTL_HOURS, // 5분의 삶의 시간이다냥
        });
    }


    /**
     * 사용자를 가져오거나 없으면 생성한다냥.
     * @param account 사용자 계정 정보다냥. ID와 이름을 포함한다냥.
     * @returns 해당 사용자 객체를 반환한다냥.
     */
    getOrCreateUser(account: Account): User {
        let user = this.users.get(account.id);
        if (!user) {
            user = new User(account);
            this.users.set(account.id, user);
        }
        return user;
    }

    async buildChatRequest(status: Entity.Status): Promise<Array<ChatMessage>> {

        
        const userConversation = this.getOrCreateUser(asAccountSanitize(status.account)).getConversation();
        const userHistoryData = userConversation.getConversation();
        const currentStatusJson = toStatusData(status);
        userConversation.addMessage('user', currentStatusJson['content']);

        const context = (await this.clientRest.getStatusContext(status.id)).data;

        const chatBody = `
${USER_DIALOGUE_PROMPT_MESSAGE}
${userHistoryData.slice(-USER_MESSAGE_HISTORY_LIMIT).map(it => '- ' + it.content).join('\n')}
${DESCENDANT_DIALOGUE_PROMPT_MESSAGE}
${context.descendants.slice(-DESCENDANT_HISTORY_LIMIT).map(it => '- ' + toStatusData(it).content).join('\n')}
${ANCESTOR_DIALOGUE_PROMPT_MESSAGE}
${context.ancestors.slice(-ANCESTOR_HISTORY_LIMIT).map(it => '- ' + toStatusData(it).content).join('\n')}
${CURRENT_DIALOGUE_PROMPT_MESSAGE}
${currentStatusJson.content}
`.trim();

        const request: ChatMessage[] = [
            { role: 'system', content: INITIAL_PROMPT_MESSAGE },
            { role: 'user', content: chatBody }
        ];
        // @ts-ignore
        return request
    }

    /**
     * 대화를 생성하여 응답한다냥.
     * @param account 사용자 계정 정보다냥. ID와 이름을 포함한다냥.
     * @param prompt 대화의 프롬프트다냥.
     * @returns 생성된 응답 문자열을 반환한다냥.
     */
    async generateResponse(status: Entity.Status): Promise<string> {
        
        const requestArray = await this.buildChatRequest(status);
        const completion = await this.openai.chat.completions.create({
            messages: requestArray,
            model: MODEL_VERSION,
            max_tokens: MAX_TOKEN,
        });

        const assistantResponse = completion.choices[0].message.content ?? "";
        return assistantResponse;
    }

    async main() {
        const selfAccount = await this.clientRest.verifyAccountCredentials();
        const selfId = selfAccount.data.id;
        const stream = this.client.userSocket();

        stream.on('connect', () => {
            console.log('냥냥! 연결됐다냥!');
        });

        stream.on('notification', async (notification) => {
            if (notification.type !== 'mention') return;

            const status: Entity.Status = notification.status;
            if (!status) return;

            console.log(`mention from ${status.account.acct}: ${status.content.slice(0, 30)}`)

            const userId = status.account.id;
            if (status.account.bot || userId === selfId) return;

            const user = this.getOrCreateUser(status.account); // 사용자를 가져온다냥
            user.resetMessageCount(); // 카운트를 리셋한다냥

            if (!user.canSendMessage()) {
                console.info(`${user.userId}`)
                this.clientRest.postStatus(`@${status.account.acct} 냥냥... 지쳤다냥. 나중에 다시 와봐냥!`, { in_reply_to_id: status.id, visibility: 'unlisted' });
                return;
            }

            try {
                const response = `@${status.account.acct} ${await this.generateResponse(status)}`;

                const posted = await this.clientRest.postStatus(response, { in_reply_to_id: status.id, visibility: 'unlisted' });
                
                const userConversation = this.getOrCreateUser(asAccountSanitize(status.account)).getConversation();
                userConversation.addMessage('assistant', '(You wrote:) ' + toStatusData(posted.data as Entity.Status).content);

                user.incrementMessageCount(); // 메시지 카운트를 증가시킨다냥
            } catch (ex) {
                console.error(ex);
            }
        });

        stream.on('error', (err: Error) => {
            console.error(err);
        });

        stream.on('heartbeat', () => {
            console.log('냥냥냥... 연결이 유지되고 있다냥!');
        });
    }
}
