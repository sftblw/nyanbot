import Account from './Account';
import { ChatMessage } from './ChatMessage';
import { USER_MESSAGE_HISTORY_LIMIT } from './EnvVars';

/**
 * 사용자 대화를 관리하는 클래스다냥.
 */
export class UserConversation {
    userId: string;
    conversation: Array<ChatMessage>;

    /**
     * 사용자 ID로 새 대화를 생성한다냥.
     * @param account 사용자 계정 정보다냥. ID와 이름을 포함한다냥.
     */
    constructor(account: Account) {
        this.userId = account.id;
        this.conversation = [];
    }

    /**
     * 대화에 메시지를 추가한다냥.
     * @param role 메시지 역할(사용자 또는 비서)다냥.
     * @param content 메시지 내용이다냥.
     */
    addMessage(role: ChatMessage["role"], content: string): void {
        this.conversation.push({ role, content });
        if (this.conversation.length > USER_MESSAGE_HISTORY_LIMIT) this.conversation = this.conversation.slice(1); // 오래된 대화를 날린다냥
    }

    /**
     * 현재 대화를 반환한다냥.
     * @returns 대화 내역을 반환한다냥.
     */
    getConversation(): Array<ChatMessage> {
        return this.conversation;
    }
}
