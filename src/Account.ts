/**
 * 계정 정보를 나타내는 인터페이스다냥.
 */
export default interface Account {
    id: string; // 사용자 ID다냥. 고유한 식별자다냥!
    username: string; // 사용자 이름이다냥. 로그인할 때 사용하는 이름이지냥!
    acct: string; // 계정 정보다냥. 일반적으로 username과 동일하다냥.
    display_name: string; // 표시할 이름이다냥. 예쁜 별명 같은 거지냥!
}


export interface ChatAccount {
    // username: string; // 사용자 이름이다냥. 로그인할 때 사용하는 이름이지냥!
    acct: string; // 계정 정보다냥. 일반적으로 username과 동일하다냥.
    display_name: string; // 표시할 이름이다냥. 예쁜 별명 같은 거지냥!
}