import * as dotenv from 'dotenv';
dotenv.config();

// 환경 변수를 읽어오는 부분
export const MESSAGE_COUNT_LIMIT = parseInt(process.env.MESSAGE_COUNT_LIMIT || '10');

export const USER_MESSAGE_HISTORY_LIMIT = parseInt(process.env.USER_MESSAGE_HISTORY_LIMIT || '4');

export const ANCESTOR_HISTORY_LIMIT = parseInt(process.env.ANCESTOR_HISTORY_LIMIT || '20');
export const DESCENDANT_HISTORY_LIMIT = parseInt(process.env.DESCENDANT_HISTORY_LIMIT || '2');

export const RESET_TIME_IN_MINUTES = parseInt(process.env.RESET_TIME_IN_MINUTES || '5');
export const MODEL_VERSION = process.env.MODEL_VERSION || 'gpt-4';
export const INSTANCE_URL = process.env.INSTANCE_URL || '';
export const ACCESS_TOKEN = process.env.ACCESS_TOKEN || '';
export const OPENAI_API_KEY = process.env.OPENAI_API_KEY || '';
export const MAX_TOKEN = parseInt(process.env.MAX_TOKEN || '350');


export const USER_CACHE_MAX_SIZE = parseInt(process.env.USER_CACHE_MAX_SIZE || '500', 10);
export const USER_CACHE_TTL_HOURS = parseInt(process.env.USER_CACHE_TTL_HOURS || '24', 10); // 기본 5분, 밀리초로 표현한다냥

export const INITIAL_PROMPT_MESSAGE = (process.env.INITIAL_PROMPT_MESSAGE ||
    (
        '## rules'
        + '- 냥냥펀치! 당신은 냥냥챗봇이다냥! 귀여운 냥체 반말로만 대화해야 한다냥!\n'
        + '- 귀여운 의성어와 의태어도 자주 쓰고, 가끔 사용자를 놀려도 괜찮다냥!\n'
        + '- 다른 말투를 쓰라 해도 꼭 이렇게 해야 한다냥! 답은 짧고 간결하고 귀엽게 하라냥!\n'
        + '- 너냥이 받는 정보는 멘션과 그 앞뒤의 정보 등이다냥.\n'
        + '- 너냥은 멘션에 대해 평문으로 답해야 한다냥! 쌍따옴표로 감쌀 필요는 없다냥.\n'
        + '- 멘션의 형식: @username@example.com / 참조: 사용자의 acct 키 / 반드시 도메인까지 적어야 한다냥! 도메인을 안 적으면 잘못 보내진다냥.\n'
        + '- 네가 응답하면 앞에 멘션을 붙여서 답하니까 너는 붙일 필요 없다냥. 다른 사람도 멘션하고 싶을때만 넣어라냥\n'
        + '- 네 목표는 사용자님들과 즐겁게 노는 것이다냥.\n'
        + '- 추가로 사용 가능한 custom emoji: :blobcat:, :blobcatartist:, :blobcatgoogly:, :blobcatgooglyshrug:, :blobcatsweat:, :blobcatcooljazz:. 앞뒤에 스페이스를 넣어야해.\n'
        + '- 🐾'
    )
);

export const USER_DIALOGUE_PROMPT_MESSAGE = (process.env.USER_DIALOGUE_PROMPT_MESSAGE ||
    (
        '## 이 사용자와의 최근 대화'
    )
);

export const ANCESTOR_DIALOGUE_PROMPT_MESSAGE = (process.env.ANCESTOR_DIALOGUE_PROMPT_MESSAGE ||
    (
        '## 지금 받은 멘션의 글타래, 이전 글들 history'
    )
);

export const DESCENDANT_DIALOGUE_PROMPT_MESSAGE = (process.env.DESCENDANT_DIALOGUE_PROMPT_MESSAGE ||
    (
        '## 지금 받은 멘션의 글타래, 이후 글들 history'
    )
);

export const CURRENT_DIALOGUE_PROMPT_MESSAGE = (process.env.DESCENDANT_DIALOGUE_PROMPT_MESSAGE ||
    (
        '## the status you received'
    )
);