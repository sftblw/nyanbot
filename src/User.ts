import Account from './Account';
import { MESSAGE_COUNT_LIMIT, RESET_TIME_IN_MINUTES } from './EnvVars';
import { UserConversation } from './UserConversation';

/**
 * 사용자와 관련된 정보를 관리하는 클래스다냥.
 */
export class User {
    userId: string;
    conversation: UserConversation;
    messageCount: number;
    lastResetTime: number;
    account: Account;

    /**
    * 사용자 계정을 기반으로 사용자를 생성한다냥.
    * @param account 사용자 계정 정보다냥. ID와 이름을 포함한다냥.
    */
    constructor(account: Account) {
        this.userId = account.id;
        this.account = account;
        this.conversation = new UserConversation(account);
        this.messageCount = 0;
        this.lastResetTime = Date.now();
    }

    /**
     * 메시지 카운트를 초기화한다냥. 5분 이상 지나면 초기화된다냥.
     */
    resetMessageCount(): void {
        if (Date.now() - this.lastResetTime > RESET_TIME_IN_MINUTES * 60 * 1000) {
            this.messageCount = 0;
            this.lastResetTime = Date.now();
        }
    }

    /**
     * 메시지 카운트를 증가시킨다냥.
     */
    incrementMessageCount(): void {
        this.messageCount++;
    }

    /**
     * 메시지를 보낼 수 있는지 확인한다냥.
     * @returns 메시지 카운트가 5 미만이면 true를 반환한다냥.
     */
    canSendMessage(): boolean {
        return this.messageCount < MESSAGE_COUNT_LIMIT;
    }

    /**
     * 현재 대화를 가져온다냥.
     * @returns 현재 대화를 반환한다냥.
     */
    getConversation(): UserConversation {
        return this.conversation;
    }
}
