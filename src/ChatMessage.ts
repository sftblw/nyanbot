/**
 * 채팅 메시지를 나타내는 인터페이스다냥.
 */
export interface ChatMessage {
    role: 'system' | 'user' | 'assistant' | 'function'; // 메시지 역할이다냥. 시스템, 사용자, 비서, 함수 중 하나냥!
    content: string; // 메시지 내용이다냥. 말하는 그 내용이지냥!
}
